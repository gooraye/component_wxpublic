# wxpublic 组件库

[![Latest Stable Version](https://poser.pugx.org/itboye/component_wxpublic/v/stable)](https://packagist.org/packages/itboye/component_wxpublic)
[![Total Downloads](https://poser.pugx.org/itboye/component_wxpublic/downloads)](https://packagist.org/packages/itboye/component_wxpublic)
[![Monthly Downloads](https://poser.pugx.org/itboye/component_wxpublic/d/monthly)](https://packagist.org/packages/itboye/component_wxpublic)
[![Daily Downloads](https://poser.pugx.org/itboye/component_wxpublic/d/daily)](https://packagist.org/packages/itboye/component_wxpublic)
[![License](https://poser.pugx.org/itboye/component_wxpublic/license)](https://packagist.org/packages/itboye/component_wxpublic)

weixin public