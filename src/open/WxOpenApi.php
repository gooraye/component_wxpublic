<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-13 15:40
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\wxpublic\open;


use by\infrastructure\helper\CallResultHelper;

/**
 * Class WxOpenApi
 * 微信开放平台统一调用接口
 * @package by\component\wxpublic\open
 */
class WxOpenApi
{
    protected $appId = "";
    protected $appSecret = "";

    function __construct($appId, $appSecret) {
        $this -> appId = $appId;
        $this -> appSecret = $appSecret;
    }

    public function  getUserInfo($code){
        $data = $this->getAccessTokenAndOpenid($code);
        //  slog($data,'','hdg');
        if(!$data['status']){
            return $data;
        }

        if(is_array($data['info'])){
            $data = $data['info'];
            $access_token = $data['access_token'];
            $openid = $data['openid'];

            $url = "https://api.weixin.qq.com/sns/userinfo?access_token=".$access_token."&openid=".$openid;

            $json = $this -> curlGet($url);

            return $json;

        }else{
            return array('status'=>false,'info'=>'未知错误!');
        }
    }

    public function getAccessTokenAndOpenid($code){
        $url_get = 'https://api.weixin.qq.com/sns/oauth2/access_token?grant_type=authorization_code&appid=' . $this -> appid . '&secret=' . $this -> appsecret.'&code='.$code;
        $json = $this -> curlGet($url_get);
        // slog($json,'','hdg');
        if(!$json['status']){
            return $json;
        }

        $data  = $json['info'];

        if(!empty($data['errmsg']) ){
            return array('status'=>false,'info'=>$data['errmsg']);
        }

        // slog($data,'','hdg');
        return array('status'=>true,'info'=>array(
            'access_token'=>$data['access_token'],
            'openid'=>$data['openid'],
            'unionid'=>$data['unionid'],
        ));

    }

    //===============================================================

    protected function curlGet($url) {
        $ch = curl_init();
        $header = "Accept-Charset: utf-8";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array($header));
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $tmpInfo = curl_exec($ch);
        $error = curl_errno($ch);
        if ($error) {
            return CallResultHelper::fail($error);
        } else {
            return CallResultHelper::success(json_decode($tmpInfo,true));
        }

    }

    protected function curlPost($url, $data) {

        $ch = curl_init();
        $header = array('Accept-Charset'=>"utf-8");
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $tmpInfo = curl_exec($ch);
        $error = curl_errno($ch);
        if ($error) {
            return array('status' => false, 'info' => $error);
        } else {
            return array('status' => true, 'info' => json_decode($tmpInfo,true));
        }
    }

}